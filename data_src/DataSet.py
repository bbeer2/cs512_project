import re
import time
import operator

class DataSet:
    def __init__(self, raw_xml_file_path):
        self.raw_xml_file_path = raw_xml_file_path
        self.articles = {}
        self.titles = {}
        self.locations = {}
        self.tags = {}
        self.sections = {}
        self.pages = {}
        self.authors = {}
        self.publications = {}
        self.abstracts = {}

        self.parse_xml(self.raw_xml_file_path)

        # self.articles[article_id]['tag'] = {}
        # self.articles[article_id]['section'] = {}
        # self.articles[article_id]['page'] = {}
        # self.articles[article_id]['author'] = {}
        # self.articles[article_id]['publication'] = {}
        # self.articles[article_id]['date'] = {}

        sorted_ids = self.get_sorted_list(self.articles)
        sorted_titles = self.get_sorted_list(self.titles)
        sorted_locations = self.get_sorted_list(self.locations)
        sorted_tags = self.get_sorted_list(self.tags)
        sorted_sections = self.get_sorted_list(self.sections)
        sorted_pages = self.get_sorted_list(self.pages)
        sorted_authors = self.get_sorted_list(self.authors)
        sorted_publications = self.get_sorted_list(self.publications)
        sorted_abstracts = self.get_sorted_list(self.abstracts)

        unique_dates = set()
        for token in self.abstracts:
            for item in self.abstracts[token]["date"]:
                unique_dates.add(item)
        sorted_dates = sorted(unique_dates)

        self.print_id_relation_to_file("id_title", self.articles, "title", sorted_ids, sorted_titles)
        self.print_id_relation_to_file("id_location", self.articles, "location", sorted_ids, sorted_locations)
        self.print_id_relation_to_file("id_tag", self.articles, "tag", sorted_ids, sorted_tags)
        self.print_id_relation_to_file("id_section", self.articles, "section", sorted_ids, sorted_sections)
        self.print_id_relation_to_file("id_page", self.articles, "page", sorted_ids, sorted_pages)
        self.print_id_relation_to_file("id_author", self.articles, "author", sorted_ids, sorted_authors)
        self.print_id_relation_to_file("id_publication", self.articles, "publication", sorted_ids, sorted_publications)
        self.print_id_relation_to_file("id_abstract", self.articles, "abstract", sorted_ids, sorted_abstracts)
        self.print_id_relation_to_file("id_date", self.articles, "date", sorted_ids, sorted_dates)

        self.print_relation_to_file("title_location", self.titles, "location", sorted_titles, sorted_locations)
        self.print_relation_to_file("title_tag", self.titles, "tag", sorted_titles, sorted_tags)
        self.print_relation_to_file("title_section", self.titles, "section", sorted_titles, sorted_sections)
        self.print_relation_to_file("title_page", self.titles, "page", sorted_titles, sorted_pages)
        self.print_relation_to_file("title_author", self.titles, "author", sorted_titles, sorted_authors)
        self.print_relation_to_file("title_publication", self.titles, "publication", sorted_titles, sorted_publications)
        self.print_relation_to_file("title_abstract", self.titles, "abstract", sorted_titles, sorted_abstracts)
        self.print_relation_to_file("title_date", self.titles, "date", sorted_titles, sorted_dates)

        self.print_relation_to_file("location_tag", self.locations, "tag", sorted_locations, sorted_tags)
        self.print_relation_to_file("location_section", self.locations, "section", sorted_locations, sorted_sections)
        self.print_relation_to_file("location_page", self.locations, "page", sorted_locations, sorted_pages)
        self.print_relation_to_file("location_author", self.locations, "author", sorted_locations, sorted_authors)
        self.print_relation_to_file("location_publication", self.locations, "publication", sorted_locations, sorted_publications)
        self.print_relation_to_file("location_abstract", self.locations, "abstract", sorted_locations, sorted_abstracts)
        self.print_relation_to_file("location_date", self.locations, "date", sorted_locations, sorted_dates)

        self.print_relation_to_file("tags_section", self.tags, "section", sorted_tags, sorted_sections)
        self.print_relation_to_file("tags_page", self.tags, "page", sorted_tags, sorted_pages)
        self.print_relation_to_file("tags_author", self.tags, "author", sorted_tags, sorted_authors)
        self.print_relation_to_file("tags_publication", self.tags, "publication", sorted_tags, sorted_publications)
        self.print_relation_to_file("tags_abstract", self.tags, "abstract", sorted_tags, sorted_abstracts)
        self.print_relation_to_file("tags_date", self.tags, "date", sorted_tags, sorted_dates)

        self.print_relation_to_file("section_page", self.sections, "page", sorted_sections, sorted_pages)
        self.print_relation_to_file("section_author", self.sections, "author", sorted_sections, sorted_authors)
        self.print_relation_to_file("section_publication", self.sections, "publication", sorted_sections, sorted_publications)
        self.print_relation_to_file("section_abstract", self.sections, "abstract", sorted_sections, sorted_abstracts)
        self.print_relation_to_file("section_date", self.sections, "date", sorted_sections, sorted_dates)

        self.print_relation_to_file("page_author", self.pages, "author", sorted_pages, sorted_authors)
        self.print_relation_to_file("page_publication", self.pages, "publication", sorted_pages, sorted_publications)
        self.print_relation_to_file("page_abstract", self.pages, "abstract", sorted_pages, sorted_abstracts)
        self.print_relation_to_file("page_date", self.pages, "date", sorted_pages, sorted_dates)

        self.print_relation_to_file("author_publication", self.authors, "publication", sorted_authors, sorted_publications)
        self.print_relation_to_file("author_abstract", self.authors, "abstract", sorted_authors, sorted_abstracts)
        self.print_relation_to_file("author_date", self.authors, "date", sorted_authors, sorted_dates)

        self.print_relation_to_file("publication_abstract", self.publications, "abstract", sorted_publications, sorted_abstracts)
        self.print_relation_to_file("publication_date", self.publications, "date", sorted_publications, sorted_dates)

        self.print_relation_to_file("abstract_date", self.abstracts, "date", sorted_abstracts, sorted_dates)

        # title_location = {}
        # all_locations = {}
        # for title in self.titles:
        #     if self.titles[title]['location']:
        #         # title_location[title] = {}
        #         for location in self.titles[title]['location']:
        #             # all_locations[location] = 1
        #             # title_location[title][location] = 1
        #             title_output.write(str(title) + " | " + str(location) +"\n")
        #
        #     if self.titles[title]['tag']:
        #         for tag in self.titles[title]['tag']:
        #             tag_output.write(str(title) + " | " + str(tag) + "\n")
        #
        #     if self.titles[title]['section']:
        #         for tag in self.titles[title]['section']:
        #             section_output.write(str(title) + " | " + str(tag) + "\n")
        #
        #     if self.titles[title]['page']:
        #         for tag in self.titles[title]['page']:
        #             page_output.write(str(title) + " | " + str(tag) + "\n")
        #
        #     if self.titles[title]['author']:
        #         for tag in self.titles[title]['author']:
        #             author_output.write(str(title) + " | " + str(tag) + "\n")
        #
        #     if self.titles[title]['publication']:
        #         for tag in self.titles[title]['publication']:
        #             publication_output.write(str(title) + " | " + str(tag) + "\n")
        #
        #     if self.titles[title]['date']:
        #         for tag in self.titles[title]['date']:
        #             date_output.write(str(title) + " | " + str(tag) + "\n")


            # sorted_titles = sorted(all_locations.items(), key=operator.itemgetter(0))

            # output_file.write("SORTED LOCATIONS:\n")
            # for idx, location in enumerate(sorted_titles):
            #     output_file.write(str(idx) + ": " + str(location[0]) + "\n")


        print "hello"
        # self.train_author_file_path = train_author_file_path
        # self.train_paper_file_path = train_paper_file_path
        #
        # self.paper_author_dict = {}
        # self.paper_conf_dict = {}
        # self.paper_term_dict = {}
        #
        # self.load_file(paper_author_file_path, self.paper_author_dict)
        # self.load_file(paper_conf_file_path, self.paper_conf_dict)
        # self.load_file(paper_term_file_path, self.paper_term_dict)
        #
        # self.author_paper_list = {}
        # self.conf_paper_list = {}
        # self.term_paper_list = {}
        #
        # self.load_file_flip(paper_author_file_path, self.author_paper_list)
        # self.load_file_flip(paper_conf_file_path, self.conf_paper_list)
        # self.load_file_flip(paper_term_file_path, self.term_paper_list)
        #
        # self.author_label = {}
        # self.conf_label = {}
        # self.paper_label = {}
        #
        # self.load_label_file(author_label_file_path, self.author_label)
        # self.load_label_file(conf_label_file_path, self.conf_label)
        # self.load_label_file(paper_label_file_path, self.paper_label)
        #
        # self.train_author_id = {}
        # self.train_paper_id = {}
        #
        # self.load_train_files()
        #
        # self.test_author_id = {}
        # self.test_paper_id = {}
        #
        # self.load_test_file(test_author_file_path, self.test_author_id)
        # self.load_test_file(test_paper_file_path, self.test_paper_id)

    def print_id_relation_to_file(self, output_file, id_relation, association, sorted_relations, sorted_associations):
        index_output_file = "../project_data/index/" + output_file + "_index.txt"

        out_index_fh = open(index_output_file, 'w')
        out_fh = open("../project_data/named/" + output_file + ".txt", 'w')

        for token in id_relation:
            if association in id_relation[token]:
                token_idx = sorted_relations.index(token)
                # print id_relation[token]
                # print id_relation[token][association]
                if isinstance(id_relation[token][association], list):
                    for item in id_relation[token][association]:
                        # item_idx = sorted_associations.index(id_relation[token][association])
                        item_idx = sorted_associations.index(item)
                        # out_fh.write(str(token) + " | " + str(id_relation[token][association]) + " | 1\n")
                        out_fh.write(str(token) + " | " + str(item) + " | 1\n")
                        out_index_fh.write(str(token_idx) + " | " + str(item_idx) + " | 1\n")
                else:
                    item_idx = sorted_associations.index(id_relation[token][association])
                    # item_idx = sorted_associations.index(item)
                    out_fh.write(str(token) + " | " + str(id_relation[token][association]) + " | 1\n")
                    # out_fh.write(str(token) + " | " + str(item) + " | 1\n")
                    out_index_fh.write(str(token_idx) + " | " + str(item_idx) + " | 1\n")

        out_fh.close()


    def print_relation_to_file(self, output_file, base_relation, association, sorted_relations, sorted_associations):
        index_output_file = "../project_data/index/" + output_file + "_index.txt"

        out_index_fh = open(index_output_file, 'w')
        out_fh = open("../project_data/named/" + output_file + ".txt", 'w')

        for token in base_relation:
            if association in base_relation[token]:
                if base_relation[token][association] is not None:
                    token_idx = sorted_relations.index(token)

                    for item in base_relation[token][association]:
                        item_idx = sorted_associations.index(item)

                        out_fh.write(str(token) + " | " + str(item) + " | " + str(base_relation[token][association][item]) + "\n")

                        out_index_fh.write(str(token_idx) + " | " + str(item_idx) + " | " + str(base_relation[token][association][item]) + "\n")

        out_fh.close()


    def parse_xml(self, file_path):
        with open(file_path) as inf:
            article_id = ""
            title = ""
            location = []
            tags = []
            section = ""
            page = ""
            authors = []
            publication = ""
            abstract = ""
            epoch_time = -1

            for line in inf:
                line = line.strip()

                title_regex = re.match("<atl>(.+)<\/atl>", line)
                article_id_regex = re.match("<header .+ uiTerm=\"(\d+)\">", line)
                location_regex = re.match("<subj type=\"geo\">(.+)<\/subj>", line)
                tag_regex = re.match("<subj type=\"unclass\">(.+)<\/subj>", line)
                section_page_regex = re.match("<ppf>([a-zA-Z]+)?(\d+)<\/ppf>", line)
                author_regex = re.match("<au>(.+)<\/au>", line)
                publication_regex = re.match("<jtl>(.+)<\/jtl>", line)
                date_regex = re.match("<dt .+>(\d+\/\d+\/\d+)<\/dt>", line)
                abstract_regex = re.match("<ab>(.+)<\/ab>", line)
                end_record_regex = re.match("<\/rec>", line)

                if article_id_regex:
                    article_id = article_id_regex.group(1)
                elif title_regex:
                    title = title_regex.group(1)
                elif location_regex:
                    location.append(location_regex.group(1))
                elif tag_regex:
                    tags.append(tag_regex.group(1))
                elif section_page_regex:
                    section = section_page_regex.group(1)
                    page = section_page_regex.group(2)
                elif author_regex:
                    authors.append(author_regex.group(1))
                elif publication_regex:
                    publication = publication_regex.group(1)
                elif date_regex:
                    raw_date = date_regex.group(1)
                    epoch_time = time.mktime(time.strptime(raw_date, "%m/%d/%Y"))
                    # test_date = time.strftime("%m/%d/%Y", time.localtime(epoch_time))
                elif abstract_regex:
                    abstract = abstract_regex.group(1)

                elif end_record_regex:
                    self.update_relations(article_id, title, location, tags, section, page, authors, publication, abstract, epoch_time)

                    article_id = ""
                    title = ""
                    location = []
                    tags = []
                    section = ""
                    page = ""
                    authors = []
                    publication = ""
                    abstract = ""
                    epoch_time = -1


    def get_sorted_list(self, relation):
        unique_tokens = set()

        for token in relation:
            unique_tokens.add(token)

        sorted_tokens = sorted(unique_tokens)

        return sorted_tokens

    def update_relations(self, article_id, title, locations, tags, section, page, authors, publication, abstract, epoch_time):
        if article_id != "":
            self.update_id_relations(article_id, title, locations, tags, section, page, authors, publication, abstract, epoch_time)

        if title != "":
            self.update_title_relations(title, locations, tags, section, page, authors, publication, abstract, epoch_time)

        if locations != []:
            for place in locations:
                self.update_location_relations(place, tags, section, page, authors, publication, abstract, epoch_time)

        if tags != []:
            for tag in tags:
                self.update_tag_relations(tag, section, page, authors, publication, abstract, epoch_time)

        if section != "" and section is not None:
            self.update_section_relations(section, page, authors, publication, abstract, epoch_time)

        if page != "":
            self.update_page_relations(page, authors, publication, abstract, epoch_time)

        if authors != []:
            for author in authors:
                self.update_authors_relations(author, publication, abstract, epoch_time)

        if publication != "":
            self.update_publication_relations(publication, abstract, epoch_time)

        if abstract != "":
            self.update_abstract_relations(abstract, epoch_time)

    def update_id_relations(self, article_id, title, location, tags, section, page, authors, publication, abstract, epoch_time):
        if article_id in self.articles:
            print article_id
            raise Exception("ERROR: There should be no duplicate articles")
        self.articles[article_id] = {}

        # self.articles[article_id]['title'] = {}
        # self.articles[article_id]['location'] = {}
        # self.articles[article_id]['tag'] = {}
        # self.articles[article_id]['section'] = {}
        # self.articles[article_id]['page'] = {}
        # self.articles[article_id]['author'] = {}
        # self.articles[article_id]['publication'] = {}
        # self.articles[article_id]['date'] = {}

        if title:
            self.articles[article_id]['title'] = title
        if location:
            self.articles[article_id]['location'] = location
        if tags:
            self.articles[article_id]['tag'] = tags
        if section:
            self.articles[article_id]['section'] = section
        if page:
            self.articles[article_id]['page'] = page
        if authors:
            self.articles[article_id]['author'] = authors
        if publication:
            self.articles[article_id]['publication'] = publication
        if abstract:
            self.articles[article_id]['abstract'] = abstract
        if epoch_time:
            self.articles[article_id]['date'] = epoch_time

    def update_title_relations(self, title, locations, tags, section, page, authors, publication, abstract, epoch_time):
        if title not in self.titles:
            self.titles[title] = {}
            self.titles[title]['location'] = {}
            self.titles[title]['tag'] = {}
            self.titles[title]['section'] = {}
            self.titles[title]['page'] = {}
            self.titles[title]['author'] = {}
            self.titles[title]['publication'] = {}
            self.titles[title]['abstract'] = {}
            self.titles[title]['date'] = {}

        for place in locations:
            self.inc_if_exists(self.titles[title]['location'], place)
        for tag in tags:
            self.inc_if_exists(self.titles[title]['tag'], tag)
        for author in authors:
            self.inc_if_exists(self.titles[title]['author'], author)

        self.inc_if_exists(self.titles[title]['section'], section)
        self.inc_if_exists(self.titles[title]['page'], page)
        self.inc_if_exists(self.titles[title]['publication'], publication)
        self.inc_if_exists(self.titles[title]['abstract'], abstract)
        self.inc_if_exists(self.titles[title]['date'], epoch_time)


    def update_location_relations(self, location, tags, section, page, authors, publication, abstract, epoch_time):
        if location not in self.locations:
            self.locations[location] = {}
            self.locations[location]['tag'] = {}
            self.locations[location]['section'] = {}
            self.locations[location]['page'] = {}
            self.locations[location]['author'] = {}
            self.locations[location]['publication'] = {}
            self.locations[location]['abstract'] = {}
            self.locations[location]['date'] = {}

        for tag in tags:
            self.inc_if_exists(self.locations[location]['tag'], tag)
        for author in authors:
            self.inc_if_exists(self.locations[location]['author'], author)

        self.inc_if_exists(self.locations[location]['section'], section)
        self.inc_if_exists(self.locations[location]['page'], page)
        self.inc_if_exists(self.locations[location]['publication'], publication)
        self.inc_if_exists(self.locations[location]['abstract'], abstract)
        self.inc_if_exists(self.locations[location]['date'], epoch_time)

    def update_tag_relations(self, tag, section, page, authors, publication, abstract, epoch_time):
        if tag not in self.tags:
            self.tags[tag] = {}
            self.tags[tag]['section'] = {}
            self.tags[tag]['page'] = {}
            self.tags[tag]['author'] = {}
            self.tags[tag]['publication'] = {}
            self.tags[tag]['abstract'] = {}
            self.tags[tag]['date'] = {}

        for author in authors:
            self.inc_if_exists(self.tags[tag]['author'], author)

        self.inc_if_exists(self.tags[tag]['section'], section)
        self.inc_if_exists(self.tags[tag]['page'], page)
        self.inc_if_exists(self.tags[tag]['publication'], publication)
        self.inc_if_exists(self.tags[tag]['abstract'], abstract)
        self.inc_if_exists(self.tags[tag]['date'], epoch_time)

    def update_section_relations(self, section, page, authors, publication, abstract, epoch_time):
        if section not in self.sections:
            self.sections[section] = {}
            self.sections[section]['page'] = {}
            self.sections[section]['author'] = {}
            self.sections[section]['publication'] = {}
            self.sections[section]['abstract'] = {}
            self.sections[section]['date'] = {}

        for author in authors:
            self.inc_if_exists(self.sections[section]['author'], author)

        self.inc_if_exists(self.sections[section]['page'], page)
        self.inc_if_exists(self.sections[section]['publication'], publication)
        self.inc_if_exists(self.sections[section]['abstract'], abstract)
        self.inc_if_exists(self.sections[section]['date'], epoch_time)

    def update_page_relations(self, page, authors, publication, abstract, epoch_time):
        if page not in self.pages:
            self.pages[page] = {}
            self.pages[page]['author'] = {}
            self.pages[page]['publication'] = {}
            self.pages[page]['abstract'] = {}
            self.pages[page]['date'] = {}

        for author in authors:
            self.inc_if_exists(self.pages[page]['author'], author)

        self.inc_if_exists(self.pages[page]['publication'], publication)
        self.inc_if_exists(self.pages[page]['abstract'], abstract)
        self.inc_if_exists(self.pages[page]['date'], epoch_time)

    def update_authors_relations(self, author, publication, abstract, epoch_time):
        if author not in self.authors:
            self.authors[author] = {}
            self.authors[author]['publication'] = {}
            self.authors[author]['abstract'] = {}
            self.authors[author]['date'] = {}

        self.inc_if_exists(self.authors[author]['publication'], publication)
        self.inc_if_exists(self.authors[author]['abstract'], abstract)
        self.inc_if_exists(self.authors[author]['date'], epoch_time)

    def update_publication_relations(self, publication, abstract, epoch_time):
        if publication not in self.publications:
            self.publications[publication] = {}
            self.publications[publication]['abstract'] = {}
            self.publications[publication]['date'] = {}

        self.inc_if_exists(self.publications[publication]['abstract'], abstract)
        self.inc_if_exists(self.publications[publication]['date'], epoch_time)

    def update_abstract_relations(self, abstract, epoch_time):
        if abstract not in self.abstracts:
            self.abstracts[abstract] = {}
            self.abstracts[abstract]['date'] = {}

        self.inc_if_exists(self.abstracts[abstract]['date'], epoch_time)

    def inc_if_exists(self, relation, term):
        if term:
            try:
                relation[term] += 1
            except KeyError:
                relation[term] = 1

def main():
    # my_data = DataSet("../data/sample.xml")
    # my_data = DataSet("../raw_data/wsj/WSJ_combined.xml")

    my_data = DataSet("../raw_data/ny_times/NY_times_combined.xml")
    
    # my_data = DataSet("dblp_data/data/PA", "dblp_data/data/PC", "dblp_data/data/PT", "dblp_data/data/author_label", "dblp_data/data/conf_label", "dblp_data/data/paper_label", "dblp_data/data/trainId_author", "dblp_data/data/trainId_paper", "dblp_data/data/testId_author", "dblp_data/data/testId_paper")
    #
    # print "Authors for paper 6216.  Expect 1, 1344, 11764"
    # for author in my_data.paper_author_dict[6216]:
    #     print str(author)
    #
    # print "Venues for paper 6216.  Expect 10"
    # for venue in my_data.paper_conf_dict[6216]:
    #     print str(venue)
    #
    # print "Terms for paper 6216.  Expect 19, 33, 60, 78, 221, 502, 683, 692, 820, 931, 1759, 1828"
    # for term in my_data.paper_term_dict[6216]:
    #     print str(term)
    #
    # print "Papers for author 1344.  Expect 369, 662, 999, 1000, 1083, ..."
    # for papers in my_data.author_paper_list[1344]:
    #     print papers
    #
    # print "Papers for venue 20.  Expect 14366->14376"
    # for papers in my_data.conf_paper_list[20]:
    #     print papers
    #
    # print "Papers for term 5.  Expect 225, 497, 500, ..., 6731, 9461, 13945"
    # for papers in my_data.term_paper_list[5]:
    #     print papers

if __name__ == "__main__":
    main()
