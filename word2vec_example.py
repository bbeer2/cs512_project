from gensim.models.word2vec import Word2Vec
import string
import nltk.data


class word2vec_test(object):


	def main(self):
		#sentences = self.parse_xml()
		sentences = self.parse_abstract()
		self.model = self.construct_model(sentences)
		self.similarity_searches()

	def parse_abstract(self):
		sentences = []
		tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
		#named/abstract_date.txt
		#section_abstract_sports.txt
		with open('named/abstract_date.txt') as f:
			for line in f:
				line = line.split('|')[0]
				sentence = tokenizer.tokenize(line.decode('utf-8'))
				if len(sentence) > 0:
					sentence = sentence[0]
					sentence = [x.strip(string.punctuation.replace('.','')) for x in sentence.lower().split()]
					#print 'SENTENCE: {}\n'.format(sentence)
					sentences += [sentence]
		return sentences

	def parse_xml(self):
		import xml.etree.ElementTree
		sentences = []
		e = xml.etree.ElementTree.parse('raw_data/titles.xml').getroot()
		for rec in e:
			sentence = [x.strip(string.punctuation) for x in rec.text.lower().split()]
			sentences += [sentence]
		return sentences

	def parse_lexisnexis_raw(self):
		sentences = []
		with open('lexisnexis_trump.txt') as f:
			for sentence in f:
				sentence = [x.strip(string.punctuation) for x in sentence.lower().split()]
				if len(sentence) > 0:
					#print 'SENTENCE: {}\n'.format(sentence)
					sentences += [sentence]
		return sentences

	def construct_model(self, sentences):
		return Word2Vec(sentences, size=100, window=5, min_count=5, workers=4)

	def similarity_searches(self):
		print self.model.similar_by_word('putin', topn=10, restrict_vocab=None)
		'''
		print 'Warriors:'
		print self.model.similar_by_word('warriors', topn=10, restrict_vocab=None)
		print 'Portland:'
		print self.model.similar_by_word('portland', topn=10, restrict_vocab=None)
		print 'Vikings:'
		print self.model.similar_by_word('vikings', topn=10, restrict_vocab=None)
		print 'Yankees'
		print self.model.similar_by_word('yankees', topn=10, restrict_vocab=None)
		'''
		#print self.most_similar('cleveland', 'golden', 'cavaliers')
		#print self.most_similar('golden', 'cleveland', 'warriors')
		#print self.most_similar('golden', 'cleveland', 'cavaliers')
		#print model.most_similar(positive=['cleveland', 'warriors'], negative=['golden'], topn=50)
		#print model.most_similar(positive=['golden', 'cavaliers'], negative=['cleveland'])
		#print self.model.most_similar(positive=['woman', 'king'], negative=['man'])



	def most_similar(self, A, B, C):
		"""
		A is to B as C is D
		A - B = C - D
		D = A - B + C
		
		returns top 10 D
		"""
		return self.model.most_similar(positive=[A, C], negative=[B], topn=10)

if __name__ == '__main__':
	W2V_test = word2vec_test()
	W2V_test.main()