import re

class DataSet:
    def __init__(self, raw_xml_file_path):
        self.raw_xml_file_path = raw_xml_file_path
        self.articles = {}
        self.titles = {}
        self.locations = {}
        self.tags = {}
        self.sections = {}
        self.pages = {}
        self.authors = {}
        self.publications = {}
        self.abstracts = {}

        self.parse_xml(self.raw_xml_file_path)

        # self.articles[article_id]['tag'] = {}
        # self.articles[article_id]['section'] = {}
        # self.articles[article_id]['page'] = {}
        # self.articles[article_id]['author'] = {}
        # self.articles[article_id]['publication'] = {}
        # self.articles[article_id]['date'] = {}

        sorted_ids = self.get_sorted_list(self.articles)
        sorted_id_dict = {}
        for idx, id in enumerate(sorted_ids):
            sorted_id_dict[id] = idx
        self.print_index_file("ids", sorted_ids)

        sorted_titles = self.get_sorted_list(self.titles)
        sorted_titles_dict = {}
        for idx, id in enumerate(sorted_titles):
            sorted_titles_dict[id] = idx
        self.print_index_file("titles", sorted_titles)

        sorted_locations = self.get_sorted_list(self.locations)
        sorted_locations_dict = {}
        for idx, id in enumerate(sorted_locations):
            sorted_locations_dict[id] = idx
        self.print_index_file("locations", sorted_locations)

        sorted_tags = self.get_sorted_list(self.tags)
        sorted_tags_dict = {}
        for idx, id in enumerate(sorted_tags):
            sorted_tags_dict[id] = idx
        self.print_index_file("tags", sorted_tags)

        sorted_sections = self.get_sorted_list(self.sections)
        sorted_sections_dict = {}
        for idx, id in enumerate(sorted_sections):
            sorted_sections_dict[id] = idx
        self.print_index_file("sections", sorted_sections)

        sorted_pages = self.get_sorted_list(self.pages)
        sorted_pages_dict = {}
        for idx, id in enumerate(sorted_pages):
            sorted_pages_dict[id] = idx
        self.print_index_file("pages", sorted_pages)

        sorted_authors = self.get_sorted_list(self.authors)
        sorted_authors_dict = {}
        for idx, id in enumerate(sorted_authors):
            sorted_authors_dict[id] = idx
        self.print_index_file("authors", sorted_authors)

        sorted_publications = self.get_sorted_list(self.publications)
        sorted_publications_dict = {}
        for idx, id in enumerate(sorted_publications):
            sorted_publications_dict[id] = idx
        self.print_index_file("publications", sorted_publications)

        sorted_abstracts = self.get_sorted_list(self.abstracts)
        sorted_abstracts_dict = {}
        for idx, id in enumerate(sorted_abstracts):
            sorted_abstracts_dict[id] = idx
        self.print_index_file("abstracts", sorted_abstracts)

        self.print_author_information("../authors_of_interest.txt", "../author_info.txt", sorted_authors, sorted_authors_dict)

        self.print_id_relation_to_file("id_title", self.articles, "title", sorted_ids, sorted_titles, sorted_titles_dict)
        self.print_id_relation_to_file("id_location", self.articles, "location", sorted_ids, sorted_locations, sorted_locations_dict)
        self.print_id_relation_to_file("id_tag", self.articles, "tag", sorted_ids, sorted_tags, sorted_tags_dict)
        self.print_id_relation_to_file("id_section", self.articles, "section", sorted_ids, sorted_sections, sorted_sections_dict)
        self.print_id_relation_to_file("id_page", self.articles, "page", sorted_ids, sorted_pages, sorted_pages_dict)
        self.print_id_relation_to_file("id_author", self.articles, "author", sorted_ids, sorted_authors, sorted_authors_dict)
        self.print_id_relation_to_file("id_publication", self.articles, "publication", sorted_ids, sorted_publications, sorted_publications_dict)
        self.print_id_relation_to_file("id_abstract", self.articles, "abstract", sorted_ids, sorted_abstracts, sorted_abstracts_dict)

        print "ID files complete"

        self.print_relation_to_file("title_location", self.titles, "location", sorted_titles, sorted_locations, sorted_locations_dict)
        self.print_relation_to_file("title_tag", self.titles, "tag", sorted_titles, sorted_tags, sorted_tags_dict)
        self.print_relation_to_file("title_section", self.titles, "section", sorted_titles, sorted_sections, sorted_sections_dict)
        self.print_relation_to_file("title_page", self.titles, "page", sorted_titles, sorted_pages, sorted_pages_dict)
        self.print_relation_to_file("title_author", self.titles, "author", sorted_titles, sorted_authors, sorted_authors_dict)
        self.print_relation_to_file("title_publication", self.titles, "publication", sorted_titles, sorted_publications, sorted_publications_dict)
        self.print_relation_to_file("title_abstract", self.titles, "abstract", sorted_titles, sorted_abstracts, sorted_abstracts_dict)

        print "title files complete"

        self.print_relation_to_file("location_tag", self.locations, "tag", sorted_locations, sorted_tags, sorted_tags_dict)
        self.print_relation_to_file("location_section", self.locations, "section", sorted_locations, sorted_sections, sorted_sections_dict)
        self.print_relation_to_file("location_page", self.locations, "page", sorted_locations, sorted_pages, sorted_pages_dict)
        self.print_relation_to_file("location_author", self.locations, "author", sorted_locations, sorted_authors, sorted_authors_dict)
        self.print_relation_to_file("location_publication", self.locations, "publication", sorted_locations, sorted_publications, sorted_publications_dict)
        self.print_relation_to_file("location_abstract", self.locations, "abstract", sorted_locations, sorted_abstracts, sorted_abstracts_dict)

        print "location files complete"

        self.print_relation_to_file("tags_section", self.tags, "section", sorted_tags, sorted_sections, sorted_sections_dict)
        self.print_relation_to_file("tags_page", self.tags, "page", sorted_tags, sorted_pages, sorted_pages_dict)
        self.print_relation_to_file("tags_author", self.tags, "author", sorted_tags, sorted_authors, sorted_authors_dict)
        self.print_relation_to_file("tags_publication", self.tags, "publication", sorted_tags, sorted_publications, sorted_publications_dict)
        self.print_relation_to_file("tags_abstract", self.tags, "abstract", sorted_tags, sorted_abstracts, sorted_abstracts_dict)
        print "tags files complete"

        self.print_relation_to_file("section_page", self.sections, "page", sorted_sections, sorted_pages, sorted_pages_dict)
        self.print_relation_to_file("section_author", self.sections, "author", sorted_sections, sorted_authors, sorted_authors_dict)
        self.print_relation_to_file("section_publication", self.sections, "publication", sorted_sections, sorted_publications, sorted_publications_dict)
        self.print_relation_to_file("section_abstract", self.sections, "abstract", sorted_sections, sorted_abstracts, sorted_abstracts_dict)
        print "section files complete"

        self.print_relation_to_file("page_author", self.pages, "author", sorted_pages, sorted_authors, sorted_authors_dict)
        self.print_relation_to_file("page_publication", self.pages, "publication", sorted_pages, sorted_publications, sorted_publications_dict)
        self.print_relation_to_file("page_abstract", self.pages, "abstract", sorted_pages, sorted_abstracts, sorted_abstracts_dict)
        print "page files complete"

        self.print_relation_to_file("author_publication", self.authors, "publication", sorted_authors, sorted_publications, sorted_publications_dict)
        self.print_relation_to_file("author_abstract", self.authors, "abstract", sorted_authors, sorted_abstracts, sorted_abstracts_dict)
        print "author files complete"

        self.print_relation_to_file("publication_abstract", self.publications, "abstract", sorted_publications, sorted_abstracts, sorted_abstracts_dict)
        print "publication files complete"

        self.print_id_title_file(sorted_id_dict)
        print "hello"

    def print_id_title_file(self, sorted_id_dict):
        out_fh = open("../../git/GNetMine/labels/id_to_verbose_title.txt", 'w')

        for idx in self.articles:
            title = self.articles[idx]["title"]
            out_fh.write(str(sorted_id_dict[idx]) + "\t" + title + "\n")


    def print_author_information(self, input_file, output_file, sorted_authors, sorted_authors_dict):
        self.author_titles = {}
        for title in self.titles:
            if 'author' in self.titles[title]:
                author_dict = self.titles[title]['author']

                for author in author_dict:
                    author_idx = sorted_authors_dict[author]

                    try:
                        self.author_titles[author_idx].append(title)
                    except KeyError:
                        self.author_titles[author_idx] = []
                        self.author_titles[author_idx].append(title)

        self.author_tags = {}
        for tag in self.tags:
            if 'author' in self.tags[tag]:
                author_dict = self.tags[tag]['author']

                for author in author_dict:
                    author_idx = sorted_authors_dict[author]

                    try:
                        self.author_tags[author_idx].append(tag)
                    except KeyError:
                        self.author_tags[author_idx] = []
                        self.author_tags[author_idx].append(tag)

        with open(input_file) as inf:
            authors_of_interest = []

            for line in inf:
                line = line.strip()

                authors_of_interest.append(int(line))

        out_fh = open(output_file, 'w')

        for author_idx in authors_of_interest:
            out_fh.write(str(author_idx) + " author: " + str(sorted_authors[int(author_idx)]) + "\n")

            out_fh.write("\tTitles:\n")
            if author_idx in self.author_titles:
                for title in self.author_titles[author_idx]:
                    out_fh.write("\t\t" + str(title) + "\n")

            out_fh.write("\tTags:\n")
            if author_idx in self.author_tags:
                for tag in self.author_tags[author_idx]:
                    out_fh.write("\t\t" + str(tag) + "\n")

    def print_index_file(self, output_file, sorted_tokens):
        out_fh = open("../../git/GNetMine/index/" + output_file + "_index_lookup.txt", 'w')

        for idx, token in enumerate(sorted_tokens):
            out_fh.write(str(idx) + "\t" + str(token) + "\n")


    def print_id_relation_to_file(self, output_file, id_relation, association, sorted_relations, sorted_associations, association_index_lookup):
        index_output_file = "../../git/GNetMine/index/" + output_file + "_index.txt"

        out_index_fh = open(index_output_file, 'w')
        out_fh = open("../../git/GNetMine/named/" + output_file + ".txt", 'w')

        size_of_relation = len(sorted_relations)
        count = 0
        num_one_percent = int(size_of_relation / 100) + 1

        for token_idx, token in enumerate(sorted_relations):
            count += 1
            if (count % num_one_percent) == 0:
                print "association: " + str(association) + " complete: " + str(count / num_one_percent)

            id_relation_token = id_relation[token]
            # if association in id_relation[token]:
            if association in id_relation_token:
                try:
                    # item_idx = sorted_associations.index(id_relation_token[association])
                    item_idx = association_index_lookup[id_relation_token[association]]
                    out_fh.write(str(token) + "\t" + str(id_relation_token[association]) + "\t1\n")
                    out_index_fh.write(str(token_idx) + "\t" + str(item_idx) + "\t1\n")
                except:
                    for item in id_relation_token[association]:
                        # item_idx = sorted_associations.index(item)
                        item_idx = association_index_lookup[item]

                        out_fh.write(str(token) + "\t" + str(item) + "\t1\n")
                        out_index_fh.write(str(token_idx) + "\t" + str(item_idx) + "\t1\n")
        out_fh.close()
        out_index_fh.close()


    def print_relation_to_file(self, output_file, base_relation, association, sorted_relations, sorted_associations, association_index_lookup):
        index_output_file = "../../git/GNetMine/index/" + output_file + "_index.txt"

        out_index_fh = open(index_output_file, 'w')
        out_fh = open("../../git/GNetMine/named/" + output_file + ".txt", 'w')

        size_of_relation = len(sorted_relations)
        count = 0
        num_one_percent = int(size_of_relation / 100) + 1

        for token_idx, token in enumerate(sorted_relations):
            count += 1
            if (count % num_one_percent) == 0:
                print "association: " + str(association) + " complete: " + str(count / num_one_percent)

            if association in base_relation[token]:
                if base_relation[token][association] is not None:
                    for item in base_relation[token][association]:
                        # item_idx = sorted_associations.index(item)
                        item_idx = association_index_lookup[item]
                        out_index_fh.write(str(token_idx) + "\t" + str(item_idx) + "\t" + str(base_relation[token][association][item]) + "\n")
                        out_fh.write(str(token) + "\t" + str(item) + "\t" + str(base_relation[token][association][item]) + "\n")
        out_index_fh.close()

    def parse_xml(self, file_path):
        with open(file_path) as inf:
            article_id = ""
            title = ""
            location = []
            tags = []
            section = ""
            page = ""
            authors = []
            publication = ""
            abstract = ""

            count = 0
            for line in inf:
                count += 1
                line = line.strip()
                if (count % 33339) == 0:
                    print "Data extraction approx percent complete: " + str((count / 33339))

                title_regex = re.match("<atl>(.+)<\/atl>", line)
                article_id_regex = re.match("<header .+ uiTerm=\"(\d+)\">", line)
                location_regex = re.match("<subj type=\"geo\">(.+)<\/subj>", line)
                tag_regex = re.match("<subj type=\"unclass\">(.+)<\/subj>", line)
                section_page_regex = re.match("<ppf>([a-zA-Z]+)?(\d+)<\/ppf>", line)
                author_regex = re.match("<au>(.+)<\/au>", line)
                publication_regex = re.match("<jtl>(.+)<\/jtl>", line)
                abstract_regex = re.match("<ab>(.+)<\/ab>", line)
                end_record_regex = re.match("<\/rec>", line)

                if article_id_regex:
                    article_id = article_id_regex.group(1)
                elif title_regex:
                    title = title_regex.group(1)
                elif location_regex:
                    location.append(location_regex.group(1))
                elif tag_regex:
                    tags.append(tag_regex.group(1))
                elif section_page_regex:
                    section = section_page_regex.group(1)
                    page = section_page_regex.group(2)
                elif author_regex:
                    authors.append(author_regex.group(1))
                elif publication_regex:
                    publication = publication_regex.group(1)
                elif abstract_regex:
                    abstract = abstract_regex.group(1)

                elif end_record_regex:
                    self.update_relations(article_id, title, location, tags, section, page, authors, publication, abstract)

                    article_id = ""
                    title = ""
                    location = []
                    tags = []
                    section = ""
                    page = ""
                    authors = []
                    publication = ""
                    abstract = ""

    def get_sorted_list(self, relation):
        unique_tokens = set()

        for token in relation:
            unique_tokens.add(token)

        sorted_tokens = sorted(unique_tokens)

        return sorted_tokens

    def update_relations(self, article_id, title, locations, tags, section, page, authors, publication, abstract):
        if article_id != "":
            self.update_id_relations(article_id, title, locations, tags, section, page, authors, publication, abstract)

        if title != "":
            self.update_title_relations(title, locations, tags, section, page, authors, publication, abstract)

        if locations != []:
            for place in locations:
                self.update_location_relations(place, tags, section, page, authors, publication, abstract)

        if tags != []:
            for tag in tags:
                self.update_tag_relations(tag, section, page, authors, publication, abstract)

        if section != "" and section is not None:
            self.update_section_relations(section, page, authors, publication, abstract)

        if page != "":
            self.update_page_relations(page, authors, publication, abstract)

        if authors != []:
            for author in authors:
                self.update_authors_relations(author, publication, abstract)

        if publication != "":
            self.update_publication_relations(publication, abstract)

        if abstract != "":
            self.update_abstract_relations(abstract)

    def update_id_relations(self, article_id, title, location, tags, section, page, authors, publication, abstract):
        if article_id in self.articles:
            print article_id
            raise Exception("ERROR: There should be no duplicate articles")
        self.articles[article_id] = {}

        if title:
            self.articles[article_id]['title'] = title
        if location:
            self.articles[article_id]['location'] = location
        if tags:
            self.articles[article_id]['tag'] = tags
        if section:
            self.articles[article_id]['section'] = section
        if page:
            self.articles[article_id]['page'] = page
        if authors:
            self.articles[article_id]['author'] = authors
        if publication:
            self.articles[article_id]['publication'] = publication
        if abstract:
            self.articles[article_id]['abstract'] = abstract

    def update_title_relations(self, title, locations, tags, section, page, authors, publication, abstract):
        if title not in self.titles:
            self.titles[title] = {}
            self.titles[title]['location'] = {}
            self.titles[title]['tag'] = {}
            self.titles[title]['section'] = {}
            self.titles[title]['page'] = {}
            self.titles[title]['author'] = {}
            self.titles[title]['publication'] = {}
            self.titles[title]['abstract'] = {}

        for place in locations:
            self.inc_if_exists(self.titles[title]['location'], place)
        for tag in tags:
            self.inc_if_exists(self.titles[title]['tag'], tag)
        for author in authors:
            self.inc_if_exists(self.titles[title]['author'], author)

        self.inc_if_exists(self.titles[title]['section'], section)
        self.inc_if_exists(self.titles[title]['page'], page)
        self.inc_if_exists(self.titles[title]['publication'], publication)
        self.inc_if_exists(self.titles[title]['abstract'], abstract)

    def update_location_relations(self, location, tags, section, page, authors, publication, abstract):
        if location not in self.locations:
            self.locations[location] = {}
            self.locations[location]['tag'] = {}
            self.locations[location]['section'] = {}
            self.locations[location]['page'] = {}
            self.locations[location]['author'] = {}
            self.locations[location]['publication'] = {}
            self.locations[location]['abstract'] = {}

        for tag in tags:
            self.inc_if_exists(self.locations[location]['tag'], tag)
        for author in authors:
            self.inc_if_exists(self.locations[location]['author'], author)

        self.inc_if_exists(self.locations[location]['section'], section)
        self.inc_if_exists(self.locations[location]['page'], page)
        self.inc_if_exists(self.locations[location]['publication'], publication)
        self.inc_if_exists(self.locations[location]['abstract'], abstract)

    def update_tag_relations(self, tag, section, page, authors, publication, abstract):
        if tag not in self.tags:
            self.tags[tag] = {}
            self.tags[tag]['section'] = {}
            self.tags[tag]['page'] = {}
            self.tags[tag]['author'] = {}
            self.tags[tag]['publication'] = {}
            self.tags[tag]['abstract'] = {}

        for author in authors:
            self.inc_if_exists(self.tags[tag]['author'], author)

        self.inc_if_exists(self.tags[tag]['section'], section)
        self.inc_if_exists(self.tags[tag]['page'], page)
        self.inc_if_exists(self.tags[tag]['publication'], publication)
        self.inc_if_exists(self.tags[tag]['abstract'], abstract)

    def update_section_relations(self, section, page, authors, publication, abstract):
        if section not in self.sections:
            self.sections[section] = {}
            self.sections[section]['page'] = {}
            self.sections[section]['author'] = {}
            self.sections[section]['publication'] = {}
            self.sections[section]['abstract'] = {}

        for author in authors:
            self.inc_if_exists(self.sections[section]['author'], author)

        self.inc_if_exists(self.sections[section]['page'], page)
        self.inc_if_exists(self.sections[section]['publication'], publication)
        self.inc_if_exists(self.sections[section]['abstract'], abstract)

    def update_page_relations(self, page, authors, publication, abstract):
        if page not in self.pages:
            self.pages[page] = {}
            self.pages[page]['author'] = {}
            self.pages[page]['publication'] = {}
            self.pages[page]['abstract'] = {}

        for author in authors:
            self.inc_if_exists(self.pages[page]['author'], author)

        self.inc_if_exists(self.pages[page]['publication'], publication)
        self.inc_if_exists(self.pages[page]['abstract'], abstract)

    def update_authors_relations(self, author, publication, abstract):
        if author not in self.authors:
            self.authors[author] = {}
            self.authors[author]['publication'] = {}
            self.authors[author]['abstract'] = {}

        self.inc_if_exists(self.authors[author]['publication'], publication)
        self.inc_if_exists(self.authors[author]['abstract'], abstract)

    def update_publication_relations(self, publication, abstract):
        if publication not in self.publications:
            self.publications[publication] = {}
            self.publications[publication]['abstract'] = {}

        self.inc_if_exists(self.publications[publication]['abstract'], abstract)

    def update_abstract_relations(self, abstract):
        if abstract not in self.abstracts:
            self.abstracts[abstract] = {}

    def inc_if_exists(self, relation, term):
        if term:
            try:
                relation[term] += 1
            except KeyError:
                relation[term] = 1

def main():
    # my_data = DataSet("../data/sample.xml")
    # my_data = DataSet("../raw_data/wsj/WSJ_combined.xml")

    # my_data = DataSet("../raw_data/ny_times/ny_times_sports_toy.xml")
    my_data = DataSet("../raw_data/ny_times/ny_times_sports_2015.xml")
    # my_data = DataSet("../raw_data/ny_times/ny_times_sports_combined.xml")

    # my_data = DataSet("../raw_data/toronto_star/toronto_star_sports.xml")

if __name__ == "__main__":
    main()
