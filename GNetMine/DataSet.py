
class DataSet:
    def __init__(self, id_title_file_path, id_loc_file_path, id_tag_file_path, id_section_file_path,
                 id_author_file_path, title_loc_file_path, title_tag_file_path, title_section_file_path,
                 title_author_file_path, loc_tag_file_path, loc_section_file_path, loc_author_file_path,
                 tag_section_file_path, tag_author_file_path, section_author_file_path, train_author_file_path,
                 train_tags_file_path, test_author_file_path, test_tags_file_path, author_label_file_path,
                 tags_label_file_path, train_id_file_path, test_id_file_path, id_label_path):

        # self.train_author_file_path = train_author_file_path
        # self.train_paper_file_path = train_paper_file_path

        self.train_author_file_path = train_author_file_path
        self.train_tags_file_path = train_tags_file_path

        self.id_title_dict = {}
        self.id_loc_dict = {}
        self.id_tag_dict = {}
        self.id_section_dict = {}
        self.id_author_dict = {}

        self.title_loc_dict = {}
        self.title_tag_dict = {}
        self.title_section_dict = {}
        self.title_author_dict = {}

        self.loc_tag_dict = {}
        self.loc_section_dict = {}
        self.loc_author_dict = {}

        self.tag_section_dict = {}
        self.tag_author_dict = {}

        self.section_author_dict = {}

        self.load_file(id_title_file_path, self.id_title_dict)
        self.load_file(id_loc_file_path, self.id_loc_dict)
        self.load_file(id_tag_file_path, self.id_tag_dict)

        self.tag_id_dict = {}
        self.load_file_flip(id_tag_file_path, self.tag_id_dict)

        self.load_file(id_section_file_path, self.id_section_dict)
        self.load_file(id_author_file_path, self.id_author_dict)

        self.load_file(title_loc_file_path, self.title_loc_dict)
        self.load_file(title_tag_file_path, self.title_tag_dict)
        self.load_file(title_section_file_path, self.title_section_dict)
        self.load_file(title_author_file_path, self.title_author_dict)

        self.load_file(loc_tag_file_path, self.loc_tag_dict)
        self.load_file(loc_section_file_path, self.loc_section_dict)
        self.load_file(loc_author_file_path, self.loc_author_dict)

        self.load_file(tag_section_file_path, self.tag_section_dict)
        self.load_file(tag_author_file_path, self.tag_author_dict)

        self.load_file(section_author_file_path, self.section_author_dict)

        # self.author_label = {}
        # self.conf_label = {}
        # self.paper_label = {}

        self.author_label = {}
        self.tags_label = {}
        self.id_label = {}

        # self.load_label_file(author_label_file_path, self.author_label)
        # self.load_label_file(conf_label_file_path, self.conf_label)
        # self.load_label_file(paper_label_file_path, self.paper_label)

        self.load_label_file(author_label_file_path, self.author_label)
        self.load_label_file(tags_label_file_path, self.tags_label)
        self.load_label_file(id_label_path, self.id_label)

        # self.train_author_id = {}
        # self.train_paper_id = {}

        self.train_author_id = {}
        self.train_tags_id = {}
        self.train_id_id = {}

        # self.load_train_files()

        self.load_train_file(train_author_file_path, self.train_author_id, self.author_label)
        self.load_train_file(train_tags_file_path, self.train_tags_id, self.tags_label)
        self.load_train_file(train_id_file_path, self.train_id_id, self.id_label)

        # self.test_author_id = {}
        # self.test_paper_id = {}

        self.test_author_id = {}
        self.test_tags_id = {}
        self.test_id_id = {}

        # self.load_test_file(test_author_file_path, self.test_author_id)
        # self.load_test_file(test_paper_file_path, self.test_paper_id)

        self.load_test_file(test_author_file_path, self.test_author_id)
        self.load_test_file(test_tags_file_path, self.test_tags_id)
        self.load_test_file(test_id_file_path, self.test_id_id)

        print "Hello"

    def invert_dictionary(self, input_dict):
        inv_dict = {}

        # for key in input_dict:
            # try:
            #     inv_dict[input_dict[key]].append(key)
            # except KeyError:
            #     inv_dict[input_dict[key]] = []
            #     inv_dict[input_dict[key]].append(key)

    def load_file(self, file_path, data_dict):
        with open(file_path) as inf:
            for line in inf:
                line_tokens = line.split()
                try:
                    data_dict[int(line_tokens[0])][int(line_tokens[1])] = int(line_tokens[2])
                except KeyError:
                    data_dict[int(line_tokens[0])] = {}
                    data_dict[int(line_tokens[0])][int(line_tokens[1])] = int(line_tokens[2])

    def load_file_flip(self, file_path, data_dict):
        with open(file_path) as inf:
            for line in inf:
                line_tokens = line.split()
                try:
                    data_dict[int(line_tokens[1])].append(int(line_tokens[0]))
                except KeyError:
                    data_dict[int(line_tokens[1])] = []
                    data_dict[int(line_tokens[1])].append(int(line_tokens[0]))

    def load_label_file(self, file_path, data_dict):
        with open(file_path) as inf:
            for line in inf:
                line_tokens = line.split()
                data_dict[int(line_tokens[0])] = int(line_tokens[1])

    def load_train_file(self, file_path, train_dict, label_dict):
        with open(file_path) as inf:
            for line in inf:
                line_tokens = line.split()
                train_dict[int(line_tokens[0])] = label_dict[(int(line_tokens[0]))]

    def load_test_file(self, file_path, data_dict):
        with open(file_path) as inf:
            for line in inf:
                line_tokens = line.split()
                data_dict[int(line_tokens[0])] = -1


def main():
    # my_data = DataSet("dblp_data/data/PA.txt", "dblp_data/data/PC.txt", "dblp_data/data/PT.txt", "dblp_data/data/author_label.txt", "dblp_data/data/conf_label.txt", "dblp_data/data/paper_label.txt", "dblp_data/data/trainId_author.txt", "dblp_data/data/trainId_paper.txt", "dblp_data/data/testId_author.txt", "dblp_data/data/testId_paper.txt")
    my_data = DataSet("index/id_title_index.txt", "index/id_location_index.txt", "index/id_tag_index.txt",
                      "index/id_section_index.txt", "index/id_author_index.txt", "index/title_location_index.txt",
                      "index/title_tag_index.txt", "index/title_section_index.txt", "index/title_author_index.txt",
                      "index/location_tag_index.txt", "index/location_section_index.txt",
                      "index/location_author_index.txt", "index/tags_section_index.txt", "index/tags_author_index.txt",
                      "index/section_author_index.txt", "train_and_label/train_authors_small.txt",
                      "train_and_label/train_tags_small.txt", "train_and_label/test_authors_small.txt",
                      "train_and_label/test_tags_small.txt", "train_and_label/author_label_small.txt",
                      "train_and_label/tags_label_small.txt", "train_and_label/train_id_small.txt",
                      "train_and_label/test_id_small.txt", "train_and_label/id_label_small.txt")

    print "Authors for article 23.  Expect 2949, 4964"
    for author in my_data.title_author_dict[23]:
        print str(author)

    # print "Venues for paper 6216.  Expect 10"
    # for venue in my_data.paper_conf_dict[6216]:
    #     print str(venue)

    print "Terms for paper 23."
    for term in my_data.title_tag_dict[23]:
        print str(term)

    # print "Papers for author 1344.  Expect 369, 662, 999, 1000, 1083, ..."
    # for papers in my_data.author_paper_list[1344]:
    #     print papers
    #
    # print "Papers for venue 20.  Expect 14366->14376"
    # for papers in my_data.conf_paper_list[20]:
    #     print papers
    #
    # print "Papers for term 5.  Expect 225, 497, 500, ..., 6731, 9461, 13945"
    # for papers in my_data.term_paper_list[5]:
    #     print papers

if __name__ == "__main__":
    main()
