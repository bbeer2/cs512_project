import DataSet
import operator
import numpy
import copy
import scipy
import h5py
import tables

class GNetMine:
    def __init__(self, data_set):
        self.data_set = data_set

        #Start with ID-Author, Title-tag, Tag-Author

        # self.author_label = {}
        # self.paper_label = {}
        self.author_label = {}
        self.tags_label = {}
        self.id_label = {}

        self.conf_est = {}
        self.conf_est['id'] = {}
        # self.conf_est['title'] = {}
        # self.conf_est['loc'] = {}
        self.conf_est['tags'] = {}
        # self.conf_est['section'] = {}
        self.conf_est['author'] = {}

        # project_data_small (NY times 2017)
        self.num_ids = 23861
        self.num_titles = 21040
        self.num_locs = 1174
        self.num_tags = 22477
        self.num_sections = 6
        self.num_authors = 2944

        # project_data_full_sports (NY times 2000-2017)
        # self.num_ids = 78474
        # self.num_titles = 69833
        # self.num_locs = 2147
        # self.num_tags = 41867
        # self.num_sections = 26
        # self.num_authors = 5823

        #Update to match the number of labels
        self.num_terms = 15

        # ***self.r_id_author = numpy.zeros(shape=(self.num_ids, self.num_authors))
        # self.r_title_tag = numpy.zeros(shape=(self.num_titles, self.num_tags))
        # ***self.r_tag_author = numpy.zeros(shape=(self.num_tags, self.num_authors))
        # self.r_id_tag = numpy.zeros(shape=(self.num_ids, self.num_tags))
        # self.r_id_tag = self.create_zeroes_array(self.stored_db, "r_id_tag", =(self.num_ids, self.num_tags))

        # self.hdf5_path = "my_extendable_compressed_data.hdf5"
        # self.stored_db = tables.open_file(self.hdf5_path, mode='w')

        self.d_id_author = numpy.zeros(shape=(self.num_ids, self.num_ids))
        # self.create_zeroes_array(self.stored_db, "d_id_author", self.num_ids, self.num_ids)
        self.d_tag_author = numpy.zeros(shape=(self.num_tags, self.num_tags))
        self.d_id_tag = numpy.zeros(shape=(self.num_ids, self.num_ids))
        # self.create_zeroes_array(self.stored_db, "d_tag_author", self.num_tags, self.num_tags)
        # self.create_zeroes_array(self.stored_db, "d_id_tag", self.num_ids, self.num_ids)

        # self.create_zeroes_array(self.stored_db, "d_tag_id", self.num_tags, self.num_tags)

        self.d_author_id = numpy.zeros(shape=(self.num_authors, self.num_authors))
        # self.d_tag_title = numpy.zeros(shape=(self.num_tags, self.num_tags))
        self.d_author_tag = numpy.zeros(shape=(self.num_authors, self.num_authors))
        self.d_tag_id = numpy.zeros(shape=(self.num_tags, self.num_tags))

        self.s_id_author = numpy.zeros(shape=(self.num_ids, self.num_authors))
        # self.s_title_tag = numpy.zeros(shape=(self.num_titles, self.num_tags))
        self.s_tag_author = numpy.zeros(shape=(self.num_tags, self.num_authors))
        # self.s_id_tag = numpy.zeros(shape=(self.num_ids, self.num_tags))


        # self.s_id_tag = self.create_zeroes_array(self.stored_db, "s_id_tag", self.num_ids, self.num_tags)
        # self.s_tag_id = self.create_zeroes_array(self.stored_db, "s_tag_id", self.num_tags, self.num_ids)

        self.s_id_tag = numpy.zeros(shape=(self.num_ids, self.num_tags))

        # self.create_zeroes_array(hdf5_file, "s_id_author", self.num_ids, self.num_authors)

        # self.stored_db.close()
        # self.stored_db = tables.open_file(self.hdf5_path, mode='a')

        # print extendable_hdf5_file.root.d_id_author[0, 0]
        # extendable_hdf5_file.root.d_id_author[0, 0] = 50
        # extendable_hdf5_file.close()
        #
        # extendable_hdf5_file = tables.open_file(hdf5_path, mode='r')
        #
        # print extendable_hdf5_file.root.d_id_author[0, 0]

        # self.r_paper_author = numpy.zeros(shape=(self.num_papers, self.num_authors))
        # self.r_paper_conf = numpy.zeros(shape=(self.num_papers, self.num_conf))
        # self.r_paper_term = numpy.zeros(shape=(self.num_papers, self.num_terms))
        # self.r_author_conf = numpy.zeros(shape=(self.num_authors, self.num_conf))
        # self.r_author_term = numpy.zeros(shape=(self.num_authors, self.num_terms))
        #
        # self.d_paper_author = numpy.zeros(shape=(self.num_papers, self.num_papers))
        # self.d_paper_conf = numpy.zeros(shape=(self.num_papers, self.num_papers))
        # self.d_paper_term = numpy.zeros(shape=(self.num_papers, self.num_papers))
        # self.d_author_conf = numpy.zeros(shape=(self.num_authors, self.num_authors))
        # self.d_author_term = numpy.zeros(shape=(self.num_authors, self.num_authors))
        #
        # self.d_author_paper = numpy.zeros(shape=(self.num_authors, self.num_authors))
        # self.d_conf_paper = numpy.zeros(shape=(self.num_conf, self.num_conf))
        # self.d_term_paper = numpy.zeros(shape=(self.num_terms, self.num_terms))
        # self.d_conf_author = numpy.zeros(shape=(self.num_conf, self.num_conf))
        # self.d_term_author = numpy.zeros(shape=(self.num_terms, self.num_terms))
        #
        # self.s_paper_author = numpy.zeros(shape=(self.num_papers, self.num_authors))
        # self.s_paper_conf = numpy.zeros(shape=(self.num_papers, self.num_conf))
        # self.s_paper_term = numpy.zeros(shape=(self.num_papers, self.num_terms))
        # self.s_author_conf = numpy.zeros(shape=(self.num_authors, self.num_conf))
        # self.s_author_term = numpy.zeros(shape=(self.num_authors, self.num_terms))

        print "constructor finished"

    def init_confidence_estimates(self):
        self.init_confidence_matrix(self.data_set.train_author_id, self.data_set.test_author_id, self.author_label, self.conf_est['author'], self.num_authors)
        self.init_confidence_matrix(self.data_set.train_tags_id, self.data_set.test_tags_id, self.tags_label, self.conf_est['tags'], self.num_tags)
        self.init_confidence_matrix(self.data_set.train_id_id, self.data_set.test_id_id, self.id_label, self.conf_est['id'], self.num_ids)
        # self.init_empty_confidence(self.conf_est['id'], self.num_ids)
        # self.init_empty_confidence(self.conf_est['title'], self.num_titles)

        # print "init_confidence_est"

    # Assumes that there are no gaps in the indexing for paper, conference or author
    def init_relation_matrices(self):
        # for author_idx in xrange(self.num_authors):
        #     for paper in self.data_set.author_paper_list[author_idx + 1]:
        #         for conf in self.data_set.paper_conf_dict[paper]:
        #             conf_idx = conf - 1
        #             self.r_author_conf[author_idx][conf_idx] = 1
        #             self.d_author_conf[author_idx][author_idx] += 1
        #             self.d_conf_author[conf_idx][conf_idx] += 1
        #
        #         for term in self.data_set.paper_term_dict[paper]:
        #             term_idx = term - 1
        #             self.r_author_term[author_idx][term_idx] = 1
        #             self.d_author_term[author_idx][author_idx] += 1
        #             self.d_term_author[term_idx][term_idx] += 1




        # for id_idx in xrange(self.num_ids):
        #     if id_idx in self.data_set.id_author_dict:
        #         for author_idx in self.data_set.id_author_dict[id_idx]:
        #             # Consider for adding weights
        #             self.d_id_author[id_idx][id_idx] += self.data_set.id_author_dict[id_idx][author_idx]
        #             self.d_author_id[author_idx][author_idx] += self.data_set.id_author_dict[id_idx][author_idx]
        #             self.r_id_author[id_idx][author_idx] = 1
        #
        #             # Consider for simplicity and matching HW
        #             # self.d_id_author[id_idx][id_idx] += 1
        #             # self.d_author_id[author_idx][author_idx] += 1

        for id_idx in xrange(self.num_ids):
            if id_idx in self.data_set.id_author_dict:
                for author_idx in self.data_set.id_author_dict[id_idx]:
                    self.s_id_author[id_idx][author_idx] = 1

                    self.d_id_author[id_idx, id_idx] += 1
                    self.d_author_id[author_idx][author_idx] += 1

            if id_idx in self.data_set.id_tag_dict:
                for tag_idx in self.data_set.id_tag_dict[id_idx]:
                    self.s_id_tag[id_idx, tag_idx] = 1

                    self.d_id_tag[id_idx, id_idx] += 1
                    self.d_tag_id[tag_idx, tag_idx] += 1


        for tag_idx in xrange(self.num_tags):
            if tag_idx in self.data_set.tag_author_dict:
                for author_idx in self.data_set.tag_author_dict[tag_idx]:
                    self.s_tag_author[tag_idx][author_idx] = 1
                    # Consider for adding weights
                    # self.d_tag_author[tag_idx][tag_idx] += self.data_set.tag_author_dict[tag_idx][author_idx]
                    # self.d_author_tag[author_idx][author_idx] += self.data_set.tag_author_dict[tag_idx][author_idx]

                    # Consider for simplicity and matching HW
                    self.d_tag_author[tag_idx][tag_idx] += 1
                    self.d_author_tag[author_idx][author_idx] += 1

        # self.s_paper_author = copy.deepcopy(self.r_paper_author)
        # self.s_paper_conf = copy.deepcopy(self.r_paper_conf)
        # self.s_paper_term = copy.deepcopy(self.r_paper_term)
        # self.s_author_conf = copy.deepcopy(self.r_author_conf)

        # self.s_id_author = copy.deepcopy(self.r_id_author)
        # self.s_tag_author = copy.deepcopy(self.r_tag_author)

        for id_idx in xrange(self.num_ids):
            d_id_author_count = self.d_id_author[id_idx, id_idx]
            if d_id_author_count > 0:
                id_author_norm = 1 / numpy.sqrt(d_id_author_count)
            else:
                id_author_norm = 1
            self.s_id_author[id_idx] = self.s_id_author[id_idx] * id_author_norm

            d_id_tag_count = self.d_id_tag[id_idx, id_idx]
            if d_id_tag_count > 0:
                id_tag_norm = 1 / numpy.sqrt(d_id_tag_count)
            else:
                id_tag_norm = 1

            self.s_id_tag[id_idx] = self.s_id_tag[id_idx] * id_tag_norm

        for author_idx in xrange(self.num_authors):
            # author_id_norm = 1 / numpy.sqrt(self.d_author_id[author_idx][author_idx])
            # self.s_id_author[:,author_idx] = self.s_id_author[:,author_idx] * author_id_norm

            d_author_tag_count = self.d_author_tag[author_idx][author_idx]
            if d_author_tag_count > 0:
                author_tag_norm =  1 / numpy.sqrt(d_author_tag_count)
            else:
                author_tag_norm = 1
            self.s_tag_author[:,author_idx] = self.s_tag_author[:,author_idx] * author_tag_norm

            d_author_id_count = self.d_author_id[author_idx][author_idx]
            if d_author_id_count > 0:
                author_id_norm =  1 / numpy.sqrt(d_author_id_count)
            else:
                author_id_norm = 1
            self.s_id_author[:,author_idx] = self.s_id_author[:,author_idx] * author_id_norm

        for tag_idx in xrange(self.num_tags):
            d_tag_author_count = self.d_tag_author[tag_idx][tag_idx]
            if d_tag_author_count > 0:
                tag_author_norm = 1 / numpy.sqrt(d_tag_author_count)
            else:
                tag_author_norm = 1
            self.s_tag_author[tag_idx] = self.s_tag_author[tag_idx] * tag_author_norm

            d_tag_id_count = self.d_tag_id[tag_idx, tag_idx]
            if d_tag_id_count > 0:
                tag_id_norm = 1 / numpy.sqrt(d_tag_id_count)
            else:
                tag_id_norm = 1
            self.s_id_tag[:,tag_idx] = self.s_id_tag[:,tag_idx] * tag_id_norm

        # for author_idx in xrange(self.num_authors):
        #     author_conf_norm = 1 / numpy.sqrt(self.d_author_conf[author_idx][author_idx])
        #     self.s_author_conf[author_idx] = self.s_author_conf[author_idx] * author_conf_norm
        #
        #     author_term_norm = 1 / numpy.sqrt(self.d_author_term[author_idx][author_idx])
        #     self.s_author_term[author_idx] = self.s_author_term[author_idx] * author_term_norm
        #
        # for paper_idx in xrange(self.num_papers):
        #     paper_author_norm = 1 / numpy.sqrt(self.d_paper_author[paper_idx][paper_idx])
        #     self.s_paper_author[paper_idx] = self.s_paper_author[paper_idx] * paper_author_norm
        #
        #     paper_conf_norm = 1 / numpy.sqrt(self.d_paper_conf[paper_idx][paper_idx])
        #     self.s_paper_conf[paper_idx] = self.s_paper_conf[paper_idx] * paper_conf_norm
        #
        #     paper_term_norm = 1 / numpy.sqrt(self.d_paper_term[paper_idx][paper_idx])
        #     self.s_paper_term[paper_idx] = self.s_paper_term[paper_idx] * paper_term_norm
        #
        # for author_idx in xrange(self.num_authors):
        #     author_norm = 1 / numpy.sqrt(self.d_author_paper[author_idx][author_idx])
        #     self.s_paper_author[:,author_idx] = self.s_paper_author[:,author_idx] * author_norm
        #
        # for conf_idx in xrange(self.num_conf):
        #     conf_paper_norm = 1 / numpy.sqrt(self.d_conf_paper[conf_idx][conf_idx])
        #     self.s_paper_conf[:,conf_idx] = self.s_paper_conf[:,conf_idx] * conf_paper_norm
        #
        #     conf_author_norm = 1 / numpy.sqrt(self.d_conf_author[conf_idx][conf_idx])
        #     self.s_author_conf[:,conf_idx] = self.s_author_conf[:,conf_idx] * conf_author_norm
        #
        # for term_idx in xrange(self.num_terms):
        #     term_paper_norm = 1 / numpy.sqrt(self.d_term_paper[term_idx][term_idx])
        #     self.s_paper_term[:,term_idx] = self.s_paper_author[:,term_idx] * term_paper_norm
        #
        #     term_author_norm = 1 / numpy.sqrt(self.d_term_author[term_idx][term_idx])
        #     self.s_author_term[:,term_idx] = self.s_author_term[:,term_idx] * term_author_norm

        print "init_relation_matrices"

    def calc_paper_class(self):
        initial_conf = copy.deepcopy(self.conf_est)

        # self.s_author_paper = numpy.transpose(self.s_paper_author)
        # self.s_term_author = numpy.transpose(self.s_author_term)
        # self.s_term_paper = numpy.transpose(self.s_paper_term)
        #
        # self.s_term_conf = numpy.dot(self.s_term_paper, self.s_paper_conf)
        #
        # self.s_conf_paper = numpy.transpose(self.s_paper_conf)
        # self.s_conf_author = numpy.transpose(self.s_author_conf)
        #
        # self.s_conf_term = numpy.transpose(self.s_term_conf)

        # Every combination of types back and forth
        self.s_author_tag = numpy.transpose(self.s_tag_author)
        self.s_author_id = numpy.transpose(self.s_id_author)
        self.s_tag_id = numpy.transpose(self.s_id_tag)

        num_paper_passes = 15
        pass_num = 0
        print "Total of " + str(num_paper_passes) + " passes"
        for i in xrange(num_paper_passes):
            pass_num += 1
            print "pass_num: " + str(pass_num)
            self.update_paper_est(initial_conf)

        label_author = {}
        for group in self.conf_est['author']:
            label_author[group] = []

        author_count = 0
        for author_idx in xrange(self.num_authors):
            max_val = -1
            assigned_group = -1
            for group in self.conf_est['author']:
                if self.conf_est['author'][group][author_idx] > max_val:
                    assigned_group = group
                    max_val = self.conf_est['author'][group][author_idx]

            label_author[assigned_group].append(author_idx)

            try:
                actual_group = self.data_set.author_label[author_idx]
            except KeyError:
                actual_group = -1

            if assigned_group == actual_group and ((author_idx) in self.data_set.test_author_id):
                author_count += 1

        print "Total authors classified correctly: " + str(author_count)

        output_file = "labels/author_label.txt"
        out_fh = open(output_file, 'w')
        for group in self.conf_est['author']:
            for author_idx in label_author[group]:
                out_fh.write(str(author_idx) + "\t" + str(group) + "\n")

        out_fh.close()


        label_tag = {}
        for group in self.conf_est['tags']:
            label_tag[group] = []

        tag_count = 0
        for tag_idx in xrange(self.num_tags):
            max_val = -1
            assigned_group = -1
            for group in self.conf_est['tags']:
                if self.conf_est['tags'][group][tag_idx] > max_val:
                    assigned_group = group
                    max_val = self.conf_est['tags'][group][tag_idx]

            label_tag[assigned_group].append(tag_idx)

            try:
                actual_group = self.data_set.tags_label[tag_idx]
            except KeyError:
                actual_group = -1

            if assigned_group == actual_group and ((tag_idx) in self.data_set.test_tags_id):
                tag_count += 1

        print "Total tags classified correctly: " + str(tag_count)

        output_file = "labels/tags_label.txt"
        out_fh = open(output_file, 'w')
        for group in self.conf_est['tags']:
            for tag_idx in label_tag[group]:
                out_fh.write(str(tag_idx) + "\t" + str(group) + "\n")

        out_fh.close()


        label_id = {}
        for group in self.conf_est['id']:
            label_id[group] = []

        id_count = 0
        for id_idx in xrange(self.num_ids):
            max_val = -1
            assigned_group = -1
            for group in self.conf_est['id']:
                if self.conf_est['id'][group][id_idx] > max_val:
                    assigned_group = group
                    max_val = self.conf_est['id'][group][id_idx]

            label_id[assigned_group].append(id_idx)

            try:
                actual_group = self.data_set.id_label[id_idx]
            except KeyError:
                actual_group = -1

            if id_idx in self.data_set.test_id_id:
                if assigned_group == actual_group:
                    id_count += 1
                else:
                    print "id: " + str(id_idx) + " misclassified as: " + str(assigned_group)

        print "Total ids classified correctly: " + str(id_count)


        output_file = "labels/id_label.txt"
        out_fh = open(output_file, 'w')
        for group in self.conf_est['id']:
            for id_idx in label_id[group]:
                out_fh.write(str(id_idx) + "\t" + str(group) + "\n")

        out_fh.close()


        # out_fh = open(output_file, 'w')

        # for paper_idx in xrange(self.num_papers):
        #     max_val = -1
        #     assigned_group = -1
        #     for group in self.conf_est['paper']:
        #         if self.conf_est['paper'][group][paper_idx] > max_val:
        #             assigned_group = group
        #             max_val = self.conf_est['paper'][group][paper_idx]
        #
        #     try:
        #         actual_group = self.data_set.paper_label[paper_idx + 1]
        #     except KeyError:
        #         actual_group = -1
        #
        #     if assigned_group == actual_group and ((paper_idx + 1) in self.data_set.test_paper_id):
        #         paper_count += 1
        #
        # print "Total papers classified correctly: " + str(paper_count)
        #
        # author_count = 0
        # for author_idx in xrange(self.num_authors):
        #     max_val = -1
        #     assigned_group = -1
        #     for group in self.conf_est['author']:
        #         if self.conf_est['author'][group][author_idx] > max_val:
        #             assigned_group = group
        #             max_val = self.conf_est['author'][group][author_idx]
        #
        #     try:
        #         actual_group = self.data_set.author_label[author_idx + 1]
        #     except KeyError:
        #         actual_group = -1
        #
        #     if assigned_group == actual_group and ((author_idx + 1) in self.data_set.test_author_id):
        #         author_count += 1
        #
        # print "Total authors classified correctly: " + str(author_count)
        #
        # conf_count = 0
        # for conf_idx in xrange(self.num_conf):
        #     max_val = -1
        #     assigned_group = -1
        #     for group in self.conf_est['conference']:
        #         if self.conf_est['conference'][group][conf_idx] > max_val:
        #             assigned_group = group
        #             max_val = self.conf_est['conference'][group][conf_idx]
        #
        #     try:
        #         actual_group = self.data_set.conf_label[conf_idx + 1]
        #     except KeyError:
        #         actual_group = -1
        #
        #     if assigned_group == actual_group:
        #         conf_count += 1
        #
        # print "Total conferences classified correctly: " + str(conf_count)
        #
        # print "Paper accuracy: " + str(paper_count / float(57))
        # print "Author accuracy: " + str(author_count / float(4014))
        # print "Conference accuracy: " + str(conf_count / float(20))

    def update_paper_est(self, initial_conf):
        last_conf_est = copy.deepcopy(self.conf_est)

        a_ij = 0.2
        alpha = 0.1

        num_data_objects = 3

        for group in self.conf_est['id']:
            self.conf_est['tags'][group] = a_ij * numpy.dot(self.s_tag_author, last_conf_est['author'][group]) + alpha * initial_conf['tags'][group]
            self.conf_est['tags'][group] += a_ij * numpy.dot(self.s_tag_id, last_conf_est['id'][group]) + alpha * initial_conf['tags'][group]
            self.conf_est['tags'][group] = self.conf_est['tags'][group] / ((num_data_objects - 1) * (a_ij + alpha))

            self.conf_est['author'][group] = a_ij * numpy.dot(self.s_author_tag, last_conf_est['tags'][group]) + alpha * initial_conf['author'][group]
            self.conf_est['author'][group] += a_ij * numpy.dot(self.s_author_id, last_conf_est['id'][group]) + alpha * initial_conf['author'][group]
            self.conf_est['author'][group] = self.conf_est['author'][group] / ((num_data_objects - 1) * (a_ij + alpha))

            self.conf_est['id'][group] = a_ij * numpy.dot(self.s_id_author, last_conf_est['author'][group]) + alpha * initial_conf['id'][group]
            self.conf_est['id'][group] += a_ij * numpy.dot(self.s_id_tag, last_conf_est['tags'][group]) + alpha * initial_conf['id'][group]
            self.conf_est['id'][group] = self.conf_est['id'][group] / ((num_data_objects - 1) * (a_ij + alpha))

            # self.conf_est['paper'][group] =

        # for group in self.conf_est['paper']:
        #     self.conf_est['paper'][group] = a_ij * numpy.dot(self.s_paper_author, last_conf_est['author'][group]) + alpha * initial_conf['paper'][group]
        #     self.conf_est['paper'][group] += a_ij * numpy.dot(self.s_paper_conf, last_conf_est['conference'][group]) + alpha * initial_conf['paper'][group]
        #     self.conf_est['paper'][group] += a_ij * numpy.dot(self.s_paper_term, last_conf_est['term'][group]) + alpha * initial_conf['paper'][group]
        #     self.conf_est['paper'][group] = self.conf_est['paper'][group] / (3 * (a_ij + alpha))
        #
        #     self.conf_est['author'][group] = a_ij * numpy.dot(self.s_author_conf, last_conf_est['conference'][group]) + alpha * initial_conf['author'][group]
        #     self.conf_est['author'][group] += a_ij * numpy.dot(self.s_author_paper, last_conf_est['paper'][group]) + alpha * initial_conf['author'][group]
        #     self.conf_est['author'][group] += a_ij * numpy.dot(self.s_author_term, last_conf_est['term'][group]) + alpha * initial_conf['author'][group]
        #     self.conf_est['author'][group] = self.conf_est['author'][group] / (3 * (a_ij + alpha))
        #
        #     self.conf_est['term'][group] = a_ij * numpy.dot(self.s_term_author, last_conf_est['author'][group]) + alpha * initial_conf['term'][group]
        #     self.conf_est['term'][group] += a_ij * numpy.dot(self.s_term_paper, last_conf_est['paper'][group]) + alpha * initial_conf['term'][group]
        #     self.conf_est['term'][group] += a_ij * numpy.dot(self.s_term_conf, last_conf_est['conference'][group]) + alpha * initial_conf['term'][group]
        #     self.conf_est['term'][group] = self.conf_est['term'][group] / (3 * (a_ij + alpha))
        #
        #     self.conf_est['conference'][group] = a_ij * numpy.dot(self.s_conf_paper, last_conf_est['paper'][group]) + alpha * initial_conf['conference'][group]
        #     self.conf_est['conference'][group] += a_ij * numpy.dot(self.s_conf_author, last_conf_est['author'][group]) + alpha * initial_conf['conference'][group]
        #     self.conf_est['conference'][group] += a_ij * numpy.dot(self.s_conf_term, last_conf_est['term'][group]) + alpha * initial_conf['conference'][group]
        #     self.conf_est['conference'][group] = self.conf_est['conference'][group] / (3 * (a_ij + alpha))

    def create_zeroes_array(self, hdf5_file, array_name, num_rows, num_columns):
        filters = tables.Filters(complevel=5, complib='blosc')
        tmp_array = numpy.zeros(num_columns, dtype=numpy.dtype(int))

        data_storage = hdf5_file.create_earray(hdf5_file.root, array_name,
                                              tables.Atom.from_dtype(tmp_array.dtype),
                                              shape=(0, num_columns),
                                              filters=filters,
                                              expectedrows=num_rows)
        for n in xrange(0, num_rows):
            tmp_array = numpy.zeros(num_columns, dtype=numpy.dtype(int))
            data_storage.append(tmp_array[None])
        # hdf5_file.close()

        # extendable_hdf5_data = extendable_hdf5_file.root.data[:]
        #
        # count = 0
        #
        # print extendable_hdf5_file.root.data[0, 0]
        # extendable_hdf5_file.root.data[0, 0] = 50
        # for row in extendable_hdf5_file.root.data.iterrows(step=1000):
        #     row[0] += 1
        #     count += 1
        #
        # extendable_hdf5_file.close()
        #
        # extendable_hdf5_file = tables.open_file(hdf5_path, mode='r')
        #
        # print extendable_hdf5_file.root.data[0, 0]
        #
        # for row in extendable_hdf5_file.root.data.iterrows(step=1000):
        #     row[0] += 1
        #     count += 1
        #
        # for idx in xrange(0, self.num_ids):
        #     tmp_array = extendable_hdf5_file.root.data[idx:(idx+1)]
        #     for outer_array in tmp_array:
        #         outer_array[0] = 1
        #
        #         # for val in outer_array:
        #         #     count += 1

    def init_confidence_matrix(self, train_id_dict, test_id_dict, label_dict, conf_est_dict, category_size):
        for token in train_id_dict:
            label_dict[token] = train_id_dict[token]

        for token in test_id_dict:
            label_dict[token] = test_id_dict[token]

        sorted_labels = sorted(label_dict.items(), key=operator.itemgetter(0))

        for label_idx in xrange(1, self.num_terms + 1):
            conf_est_dict[label_idx] = numpy.zeros(shape=(category_size, 1))

        for label_idx in xrange(1, self.num_terms + 1):
            for token in sorted_labels:
                if token[1] == label_idx:
                    conf_est_dict[label_idx][token[0] - 1] = 1
                    # try:
                    #     conf_est_dict[label_idx][token[0] - 1] = 1
                    # except KeyError:
                    #     conf_est_dict[label_idx] = numpy.zeros(shape=(category_size, 1))
                    #     conf_est_dict[label_idx][token[0] - 1] = 1

    def init_empty_confidence(self, conf_est_dict, category_size):
        for label_idx in xrange(1, self.num_terms + 1):
            conf_est_dict[label_idx] = numpy.zeros(shape=(category_size, 1))

def main():
    # my_data = DataSet.DataSet("dblp_data/data/PA.txt", "dblp_data/data/PC.txt", "dblp_data/data/PT.txt", "dblp_data/data/author_label.txt", "dblp_data/data/conf_label.txt", "dblp_data/data/paper_label.txt", "dblp_data/data/trainId_author.txt", "dblp_data/data/trainId_paper.txt", "dblp_data/data/testId_author.txt", "dblp_data/data/testId_paper.txt")

    my_data = DataSet.DataSet("index/id_title_index.txt", "index/id_location_index.txt", "index/id_tag_index.txt",
                      "index/id_section_index.txt", "index/id_author_index.txt", "index/title_location_index.txt",
                      "index/title_tag_index.txt", "index/title_section_index.txt", "index/title_author_index.txt",
                      "index/location_tag_index.txt", "index/location_section_index.txt",
                      "index/location_author_index.txt", "index/tags_section_index.txt", "index/tags_author_index.txt",
                      "index/section_author_index.txt", "train_and_label/train_authors_small.txt",
                      "train_and_label/train_tags_small.txt", "train_and_label/test_authors_small.txt",
                      "train_and_label/test_tags_small.txt", "train_and_label/author_label_small.txt",
                      "train_and_label/tags_label_small.txt", "train_and_label/train_id_small.txt",
                      "train_and_label/test_id_small.txt", "train_and_label/id_label_small.txt")

    gnet_miner = GNetMine(my_data)
    gnet_miner.init_confidence_estimates()
    gnet_miner.init_relation_matrices()
    gnet_miner.calc_paper_class()


if __name__ == "__main__":
    main()
