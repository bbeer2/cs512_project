from gensim.models.word2vec import Word2Vec
from scipy import sparse
from numpy import array
import os
import sys
import argparse
import csv


class DataParser(object):
	def __init__(self):
		self.load_index_data()
		self.load_gnetmine_data()
		self.load_dimensions_and_lookup()
		
	def load_index_data(self):
		# Id
		self.id_abstract_index = self.data_to_coomatrix('data/index/id_abstract_index.txt')
		self.id_author_index = self.data_to_coomatrix('data/index/id_author_index.txt')
		self.id_location_index = self.data_to_coomatrix('data/index/id_location_index.txt')
		self.id_section_index = self.data_to_coomatrix('data/index/id_section_index.txt')
		self.id_tag_index = self.data_to_coomatrix('data/index/id_tag_index.txt')
		# Location (l)
		self.location_abstract_index = self.data_to_coomatrix('data/index/location_abstract_index.txt')
		self.location_author_index = self.data_to_coomatrix('data/index/location_author_index.txt')
		self.location_section_index = self.data_to_coomatrix('data/index/location_section_index.txt')
		self.location_tag_index = self.data_to_coomatrix('data/index/location_tag_index.txt')
		# Tags (ta)
		self.tags_abstract_index = self.data_to_coomatrix('data/index/tags_abstract_index.txt')
		self.tags_author_index = self.data_to_coomatrix('data/index/tags_author_index.txt')
		self.tags_section_index = self.data_to_coomatrix('data/index/tags_section_index.txt')
		# Section (s)
		self.section_abstract_index = self.data_to_coomatrix('data/index/section_abstract_index.txt')
		self.section_author_index = self.data_to_coomatrix('data/index/section_author_index.txt')
		# Author (au)
		self.author_abstract_index = self.data_to_coomatrix('data/index/author_abstract_index.txt')
		# Abstract (ab) - Already fully covered by prevoius imports

	def load_gnetmine_data(self):
		self.author_label_index = self.data_to_coomatrix('data/labels/author_label.txt')
		self.id_label_index = self.data_to_coomatrix('data/labels/id_label.txt')
		self.tags_label_index = self.data_to_coomatrix('data/labels/tags_label.txt')
		# Lookup table
		self.label_lookup = self.create_lookup_table('label_index_lookup.txt')
		# Dimension
		self.label_dim = len(self.label_lookup)

	def create_lookup_table(self, filename):
		lookup_table = {}
		with open(filename) as f:
			for line in f.readlines():
				elements = line.split('\t', 1)
				lookup_table[int(elements[0])] = elements[1]
		return lookup_table

	def load_dimensions_and_lookup(self):
		# Lookup tables
		self.abstract_lookup = self.create_lookup_table('data/index/abstracts_index_lookup.txt')
		self.author_lookup = self.create_lookup_table('data/index/authors_index_lookup.txt')
		self.location_lookup = self.create_lookup_table('data/index/locations_index_lookup.txt')
		self.section_lookup = self.create_lookup_table('data/index/sections_index_lookup.txt')
		self.tag_lookup = self.create_lookup_table('data/index/tags_index_lookup.txt')
		self.id_lookup = self.create_lookup_table('data/labels/id_to_verbose_title.txt')
		# Dimensions
		self.abstract_dim = len(self.abstract_lookup)
		self.author_dim = len(self.author_lookup)
		self.location_dim = len(self.location_lookup)
		self.section_dim = len(self.section_lookup)
		self.tag_dim = len(self.tag_lookup)
		self.id_dim = len(self.id_lookup)

	def lookup(self, type, id):
		if type == 'abstract':
			return self.abstract_lookup[id]
		elif type == 'author':
			return self.author_lookup[id]
		elif type == 'location':
			return self.location_lookup[id]
		elif type == 'section':
			return self.section_lookup[id]
		elif type == 'tag':
			return self.tag_lookup[id]
		elif type == 'id':
			return self.id_lookup[id]
		elif type == 'label':
			return self.label_lookup[id]
		else:
			return None

	def data_to_coomatrix(self, filename):
	    I = []
	    J = []
	    V = []
	    with open(filename) as f:
	        for line in f.readlines():
	            elements = line.split()
	            I.append(int(elements[0]))
	            J.append(int(elements[1]))
	            if len(elements) == 3:
	            	V.append(int(elements[2]))
	            else:
	            	# When no weight is provided assume weight of 1
	            	V.append(1)
	    I = array(I)
	    J = array(J)
	    V = array(V)
	    return sparse.coo_matrix((V, (I, J)))


class Node2VecExample(object):
	def __init__(self, data):
		self.data = data
		self.type_offsets = {
			'abstract': 0,
			'author': data.abstract_dim,
			'location': data.abstract_dim + data.author_dim,
			'section': data.abstract_dim + data.author_dim + data.location_dim,
			'tag': data.abstract_dim + data.author_dim + data.location_dim + data.section_dim,
			'id': data.abstract_dim + data.author_dim + data.location_dim + data.section_dim + data.tag_dim,
			'label': data.abstract_dim + data.author_dim + data.location_dim + data.section_dim + data.tag_dim + data.id_dim,
		}
		self.edgelists = [
			# Edgelist-0 - id, Author
			[(self.data.id_author_index, 'id', 'author')],
			# Edgelist-1
			[(self.data.id_author_index, 'id', 'author'),
			## GNetMine
			(self.data.id_label_index, 'id', 'label'),],

			# Edgelist-2 - id, Author, Tag
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),],
			# Edgelist-3 - Just Id label
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),
			## GNetMine
			(self.data.id_label_index, 'id', 'label'),],

			# Edgelist-4 - id, Author, Tag
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),],
			# Edgelist-5 - Id & Tag labels
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),
			## GNetMine
			(self.data.id_label_index, 'id', 'label'),
			(self.data.tags_label_index, 'tag', 'label'),],

			# Edgelist-6 - id, Author, Tag
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),],
			# Edgelist-7 - Id, Tag & Author labels
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),
			## GNetMine
			(self.data.author_label_index, 'author', 'label'),
			(self.data.id_label_index, 'id', 'label'),
			(self.data.tags_label_index, 'tag', 'label'),],

			# Edgelist-8 - id, Author, Tag, Location
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),
			(self.data.id_location_index, 'id', 'location'),
			(self.data.location_author_index, 'location', 'author'),
			(self.data.location_tag_index, 'location', 'tag'),],
			# Edgelist-9
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.tags_author_index, 'tag', 'author'),
			(self.data.id_location_index, 'id', 'location'),
			(self.data.location_author_index, 'location', 'author'),
			(self.data.location_tag_index, 'location', 'tag'),
			## GNetMine
			(self.data.author_label_index, 'author', 'label'),
			(self.data.id_label_index, 'id', 'label'),
			(self.data.tags_label_index, 'tag', 'label'),],

			# Edgelist-10 - id, Author, Tag, Location, Section
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.id_location_index, 'id', 'location'),
			(self.data.location_author_index, 'location', 'author'),
			(self.data.id_section_index, 'id', 'section'),
			(self.data.location_section_index, 'location', 'section'),
			(self.data.tags_author_index, 'tag', 'author'),
			(self.data.tags_section_index, 'tag', 'section'),
			(self.data.section_author_index, 'section', 'author'),],
			# Edgelist-11
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_tag_index, 'id', 'tag'),
			(self.data.id_location_index, 'id', 'location'),
			(self.data.location_author_index, 'location', 'author'),
			(self.data.id_section_index, 'id', 'section'),
			(self.data.location_section_index, 'location', 'section'),
			(self.data.tags_author_index, 'tag', 'author'),
			(self.data.tags_section_index, 'tag', 'section'),
			(self.data.section_author_index, 'section', 'author'),
			## GNetMine
			(self.data.author_label_index, 'author', 'label'),
			(self.data.id_label_index, 'id', 'label'),
			(self.data.tags_label_index, 'tag', 'label'),],

			# Edgelist-12 - id, Author, Location, Section
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_location_index, 'id', 'location'),
			(self.data.location_author_index, 'location', 'author'),
			(self.data.id_section_index, 'id', 'section'),
			(self.data.location_section_index, 'location', 'section'),
			(self.data.section_author_index, 'section', 'author'),],

			# Edgelist-13
			[(self.data.id_author_index, 'id', 'author'),
			(self.data.id_location_index, 'id', 'location'),
			(self.data.location_author_index, 'location', 'author'),
			(self.data.id_section_index, 'id', 'section'),
			(self.data.location_section_index, 'location', 'section'),
			(self.data.section_author_index, 'section', 'author'),
			## GNetMine
			(self.data.author_label_index, 'author', 'label'),
			(self.data.id_label_index, 'id', 'label'),],
		]

	def type_check(self, id):
		if self.type_offsets['abstract'] <= id < self.type_offsets['author']:
			return 'abstract'
		elif self.type_offsets['author'] <= id < self.type_offsets['location']:
			return 'author'
		elif self.type_offsets['location'] <= id < self.type_offsets['section']:
			return 'location'
		elif self.type_offsets['section'] <= id < self.type_offsets['section']:
			return 'section'
		elif self.type_offsets['tag'] <= id < self.type_offsets['id']:
			return 'tag'
		elif self.type_offsets['id'] <= id < self.type_offsets['label']:
			return 'id'
		elif self.type_offsets['label'] <= id <= self.type_offsets['label'] + self.data.label_dim:
			return 'label'
		else:
			return 'unknown'

	def generate_network(self, filename):
		for index, edgelist in enumerate(self.edgelists):
			with open('{}-{}'.format(filename, index), 'w') as f:
				for item in edgelist:
					matrix, type1, type2 = item
					f.write(self.write_edgelist(matrix, type1, type2))

	def write_edgelist(self, matrix, type1, type2):
		string_buffer = ''
		for i,j,v in zip(matrix.row, matrix.col, matrix.data):
			string_buffer += '{i}\t{j}\t{v}\n'.format(i=(i + self.type_offsets[type1]), j=(j + self.type_offsets[type2]), v=v)
		return string_buffer

	def load_embedding(self, filename):
		return Word2Vec.load_word2vec_format(filename)

	def convert_emdid_to_id_type(self, emd_id):
		type = self.type_check(emd_id)
		id = emd_id - self.type_offsets[type]
		return id, type

	def convert_id_type_to_emdid(self, id, type):
		emd_id = str(id + self.type_offsets[type])
		return emd_id

	def evaluate_emd_id(self, model, emd_id):
		id, type = self.convert_emdid_to_id_type(int(emd_id))
		print 'Query type: {}, ID: {}'.format(type, id)
		print self.data.lookup(type, id)
		result = model.similar_by_word(emd_id, topn=10)
		for result_emd_id, similarity in result:
			id, type = self.convert_emdid_to_id_type(int(result_emd_id))
			print '\tResult type: {}, ID: {}, similarity: {}'.format(type, id, similarity)
			print '\t\t{}'.format(self.data.lookup(type, id))
		return result

	def evaluate_id_type(self, model, id, type):
		emd_id = self.convert_id_type_to_emdid(id, type)
		return self.evaluate_emd_id(model, emd_id)

	def evaluate_modellist_emd_id(self, output_file, models, emd_id, restrict_type=None, restrict_type_topn=10, topn=10):
		id, type = self.convert_emdid_to_id_type(int(emd_id))
		output_file.writerow(['Query type: {}'.format(type), 'ID: {}'.format(id), 'Data: {}'.format(self.data.lookup(type, id))])
		output_file.writerow(['Embedding File'] + ['Type','ID','Data','Similarity'] * topn)
		for index, model in enumerate(models):
			results_row = [index]
			try:
				result = model.similar_by_word(emd_id, topn=topn)
				for result_emd_id, similarity in result:
					id, type = self.convert_emdid_to_id_type(int(result_emd_id))
					# If we want to restrict type and we do not have the type we desire, skip to the next iteration
					if restrict_type:
						if type != restrict_type:
							continue
						if results_row == restrict_type_topn:
							break
					data = self.data.lookup(type, id)
					results_row += [type, id, data, similarity]
			except KeyError:
				results_row += ['N/A', 'N/A', 'N/A', 'N/A'] * topn
			output_file.writerow(results_row)

	def evaluate_modellist_id_type(self, output_file, models, id, type, restrict_type=None, restrict_type_topn=10, topn=10):
		emd_id = self.convert_id_type_to_emdid(id, type)
		self.evaluate_modellist_emd_id(output_file, models, emd_id, restrict_type, restrict_type_topn, topn)

def main(argv):
	parser = argparse.ArgumentParser(add_help=True)
	parser.add_argument('-g','--generate',help='Generate edgelist file name, to be processed by node2vec', required=False)
	parser.add_argument('-e','--evaluate', help='Evaluate embedding file root name (e.g. for embedding files labled graph.emd-0, graph.emd-1, etc. use \'graph.emd\'); produce \'results.csv\' files',required=False)
	args = parser.parse_args()

	if not args.generate and not args.evaluate:
		parser.print_help()
		sys.exit(2)

	data = DataParser()
	n2v = Node2VecExample(data)
	if args.generate:
		edgelist_file = args.generate
		n2v.generate_network(edgelist_file)
	# TODO System call to shell script or calling Node2Vec function req'd to generated embedding file
	if args.evaluate:
		embedding_file = args.evaluate
		evaluate_all_embeddings(n2v, embedding_file)

def evaluate_all_embeddings(n2v, embedding_file):
	blank_lines = 10
	top_50_authors = [2475,552,2612,1342,336,1428,2838,2924,1441,2275,344,249,189,2671,496,2640,2531,367,626,329,2820,2751,1997,564,745,425,2075,1590,600,681,2733,1743,2166,2308,688,2731,1331,391,2358,66,1536,2661,1611,1647,1361,1,125,2489,377,1421]

	filename = 'results.csv'
	with open(filename, 'wb') as csvfile:
		outfile = csv.writer(csvfile)
		models = []
		for index in range(len(n2v.edgelists)): 
			models += [n2v.load_embedding('{}-{}'.format(embedding_file, index))]

		# Top 50 authors
		for author_id in top_50_authors:
			n2v.evaluate_modellist_id_type(outfile, models, author_id, 'author')
		for index in range(blank_lines):
			outfile.writerow([])

		# Top 50 authors - authors only
		for author_id in top_50_authors:
			n2v.evaluate_modellist_id_type(outfile, models, author_id, 'author', restrict_type='author', topn=1000)
		for index in range(blank_lines):
			outfile.writerow([])

		# 50 random articles
		for id in range(0, 5000, 100):
			n2v.evaluate_modellist_id_type(outfile, models, id, 'id')
		for index in range(blank_lines):
			outfile.writerow([])

		# All labels
		for label_id in range(1,16):
			n2v.evaluate_modellist_id_type(outfile, models, label_id, 'label')
		for index in range(blank_lines):
			outfile.writerow([])

		# All labels - title
		for label_id in range(1,16):
			n2v.evaluate_modellist_id_type(outfile, models, label_id, 'label', restrict_type='title', topn=1000)
		for index in range(blank_lines):
			outfile.writerow([])

		# All labels - author
		for label_id in range(1,16):
			n2v.evaluate_modellist_id_type(outfile, models, label_id, 'label', restrict_type='author', topn=1000)
		for index in range(blank_lines):
			outfile.writerow([])

		# All labels - tag
		for label_id in range(1,16):
			n2v.evaluate_modellist_id_type(outfile, models, label_id, 'label', restrict_type='tag', topn=1000)
		for index in range(blank_lines):
			outfile.writerow([])

		# 20 random locations
		for location_id in range(0, 200, 10):
			n2v.evaluate_modellist_id_type(outfile, models, location_id, 'location')
		for index in range(blank_lines):
			outfile.writerow([])

		# 20 random locations - location
		for location_id in range(0, 200, 10):
			n2v.evaluate_modellist_id_type(outfile, models, location_id, 'location', restrict_type='location', topn=1000)
		for index in range(blank_lines):
			outfile.writerow([])


		# 20 random tags
		for tag_id in range(0, 200, 10):
			n2v.evaluate_modellist_id_type(outfile, models, tag_id, 'tag')
		for index in range(blank_lines):
			outfile.writerow([])

if __name__ == '__main__':
	main(sys.argv[1:])
